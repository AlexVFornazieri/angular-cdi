package br.com.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author barizon
 */
@Entity
@Table(name = "venda")
public class Venda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "venda_id")
    private Long id;
    @Column(name = "venda_data")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data = new Date();
    @Column(name = "venda_total")
    private BigDecimal total;
    @Column(name = "venda_total_liquido")
    private BigDecimal totalLiquido;
    @Column(name = "venda_desconto")
    private BigDecimal desconto;
    @ManyToOne
    @JoinColumn(name = "cliente_id", nullable = false)
    private Cliente cliente;
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "venda",
            fetch = FetchType.LAZY,
            orphanRemoval = true)
    private List<VendaItem> vendaItens = new ArrayList<>();

    public void adicionarItem(VendaItem item) {
        if (vendaItens.contains(item)) {
            int index = vendaItens.indexOf(item);
            VendaItem ia = vendaItens.get(index);
            ia.setQuantidade(ia.getQuantidade()
                    .add(item.getQuantidade()));
        } else {
            item.setVenda(this);
            vendaItens.add(item);
        }
    }

    public void removerItem(VendaItem item) {
        vendaItens.remove(item);
    }

    public void calculaTotal() {
        total = BigDecimal.ZERO;
        for (VendaItem i : vendaItens) {
            total = i.getPreco().multiply(i.getQuantidade());
        }
        totalLiquido = total.subtract(desconto);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<VendaItem> getVendaItens() {
        return vendaItens;
    }

    public void setVendaItens(List<VendaItem> vendaItens) {
        this.vendaItens = vendaItens;
    }

    public BigDecimal getTotalLiquido() {
        return totalLiquido;
    }

    public void setTotalLiquido(BigDecimal totalLiquido) {
        this.totalLiquido = totalLiquido;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Venda other = (Venda) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id.toString();
    }

}
