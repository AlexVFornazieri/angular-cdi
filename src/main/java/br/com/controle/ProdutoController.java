package br.com.controle;

import br.com.entidade.Produto;
import br.com.service.ProdutoService;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path(value = "/produto")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProdutoController implements Serializable {
    @Inject
    private ProdutoService produtoService;   

    @GET
    @Path("/listagem")
    public List<Produto> listagem() {
        return produtoService.listar();
    }

    @POST
    @Path("/salvar")
    public void salvar(Produto p) throws Exception {
        produtoService.salvar(p);
    }

    @POST
    @Path("/excluir")
    public void excluir(Produto p) {
        produtoService.excluir(p);
    }

}
