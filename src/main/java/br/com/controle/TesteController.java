package br.com.controle;

import br.com.entidade.Teste;
import br.com.service.TesteService;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path(value = "/teste")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TesteController implements Serializable {
    @Inject
    private TesteService testeService;   

    @GET
    @Path("/listagem")
    public List<Teste> listagem() {
        return testeService.listar();
    }

    @POST
    @Path("/salvar")
    public void salvar(Teste t) {
        testeService.salvar(t);
    }

    @POST
    @Path("/excluir")
    public void excluir(Teste t) {
        testeService.excluir(t);
    }

}
